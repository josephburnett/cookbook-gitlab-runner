# frozen_string_literal: true

require 'chef/provider/apt_repository'
require "chef/resource/apt_repository"

# This improves the apt-key handling of
# https://github.com/chef/chef/blob/main/lib/chef/resource/apt_repository.rb
# to account for GPG keys that have their expiration dates extended. These
# keys may have the same fingerprint so they are not added by the default
# Chef implementation.
class Chef
  class Resource
    class AptRepositoryWithKeyExpiry < AptRepository
      resource_name :apt_repository_with_key_expiry
      provides(:apt_repository_with_key_expiry) { true }
    end
  end
end

class Chef
  class Provider
    class AptRepositoryWithKeyExpiry < AptRepository
      provides :apt_repository_with_key_expiry, platform_family: "debian"

      LIST_APT_KEY_PUBLIC_KEYS = %w[apt-key adv --list-public-keys --with-colons].freeze

      def no_new_keys?(file)
        # Now we are using the option --with-colons that works across old os versions
        # as well as the latest (16.10). This for both `apt-key` and `gpg` commands
        installed_keys = extract_public_keys_from_cmd(*LIST_APT_KEY_PUBLIC_KEYS)
        proposed_keys = extract_public_keys_from_cmd('gpg', '--with-colons', file)
        (installed_keys & proposed_keys).sort == proposed_keys.sort
      end

      def extract_public_keys_from_cmd(*cmd)
        so = shell_out(*cmd)
        # Sample output
        # pub:e:4096:1:14219A96E15E78F4:2015-04-17:2020-04-15::-:GitLab B.V. (package repository signing key) <packages@gitlab.com>::sc:
        # pub:-:4096:1:3F01618A51312F3F:2020-03-02:2024-03-01::-:GitLab B.V. (package repository signing key) <packages@gitlab.com>::scESC:
        #
        # Newer gpg versions use UNIX timestamps instead of the date.
        so.stdout.split(/\n/).map do |t|
          z = t.match(/(^pub:.?:\d*:\d*:\w*:[\d-]*:[\d-]*):/)
          z[1] if z
        end.compact
      end
    end
  end
end
