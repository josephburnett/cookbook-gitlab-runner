# Variables
KEY_FILE := $$HOME/.ssh/id_ed25519

define ssh_config
Host *
	StrictHostKeyChecking	no
endef
export ssh_config

.PHONY: kitchen
kitchen:	### Run kitchen tests on GCP
ifeq ($(GITLAB_CI),)
	@# Locally, just fire up kitchen test, as we're not using ephemeral keys
	bundle exec kitchen test --destroy=always $(KITCHEN_SUITE)
else
	@# On CI, wrap kitchen test into setup/cleanup key routines

	@# Check for GCP service account
	@if [ -z "$$GCP_SERVICE_ACCOUNT" ]; then \
		echo "Please set GCP_SERVICE_ACCOUNT in CI/CD settings for this repo"; \
		exit 1; \
	fi

	@# Create the service account credential file
	mkdir -p $$HOME/.config/gcloud/ && \
		cp "$$GCP_SERVICE_ACCOUNT" $$HOME/.config/gcloud/application_default_credentials.json

	@# Disable strict host checking and generate ephemeral key
	umask 0077 && \
		mkdir -p $$HOME/.ssh && \
		echo "$$ssh_config" > $$HOME/.ssh/config && \
		ssh-keygen -N '' -t ed25519 -C '' -f "$(KEY_FILE)"

	@# Run kitchen test, wrapped in key setup/destroy routines
	export SSH_KEY="$(KEY_FILE)" \
		export KITCHEN_YAML=kitchen.ci.yml; \
		bundle exec kitchen test --destroy=always --concurrency=8 $(KITCHEN_SUITE); \
		r=$$?; \
		exit $$r	# and passing kitchen error, so that we still fail pipeline if its not zero \
				# and if key deletion fails, Makefile will exit with error and tell us.
endif
