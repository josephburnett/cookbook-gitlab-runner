name             'cookbook-gitlab-runner'
maintainer       'GitLab Inc.'
maintainer_email 'marin@gitlab.com'
license          'All rights reserved'
description      'Installs/Configures gitlab-runner'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.4.0'


depends 'apt', '~> 7.2'
depends 'yum', '~> 6.1'
