Cookbook GitLab Runner
===============================
This cookbook is used to setup and configure GitLab runner.


## Recipes

> default.rb

Default recipe which loads all the existing recipes

> docker_install.rb

Install docker-ce and docker-machine.

> runner_install.rb

Install gitlab-ci-multi-runner.

> runner_configure.rb

Configure `config.toml` of gitlab-ci-multi-runner.

## Configuration

See the [Configuration Readme](doc/configuration/README.md).

## Known limitations

- Due to the missing option of fetching a runner token it is currently not
possible to set the runner token.

License and Authors
-------------------
Authors: Marin Jankovski
