default['cookbook-gitlab-runner']['config_dir'] = '/etc/gitlab-runner'
default['cookbook-gitlab-runner']['config_path'] = "#{node['cookbook-gitlab-runner']['config_dir']}/config.toml"
default['cookbook-gitlab-runner']['cloudinit_path'] = "#{node['cookbook-gitlab-runner']['config_dir']}/cloudinit.sh"
default['cookbook-gitlab-runner']['cloudinit_swap_path'] = "#{node['cookbook-gitlab-runner']['config_dir']}/cloudinit-swap.sh"

default['cookbook-gitlab-runner']['docker-ce']['install'] = true
default['cookbook-gitlab-runner']['docker-ce']['listen-address'] = '0.0.0.0:9323'

default['cookbook-gitlab-runner']['docker-machine']['install'] = true
default['cookbook-gitlab-runner']['docker-machine']['version'] = "0.13.0"
default['cookbook-gitlab-runner']['docker-machine']['source'] = "https://github.com/docker/machine/releases/download/v#{node['cookbook-gitlab-runner']['docker-machine']['version']}/docker-machine-Linux-x86_64"
default['cookbook-gitlab-runner']['docker-machine']['checksum'] = "8f5310eb9e04e71b44c80c0ccebd8a85be56266b4170b4a6ac6223f7b5640df9"

default['cookbook-gitlab-runner']['gcp']['service_account_file'] = "#{node['cookbook-gitlab-runner']['config_dir']}/service-account.json"
default['cookbook-gitlab-runner']['gcp']['service_account'] = {}

default['cookbook-gitlab-runner']['keys'] = []

default['cookbook-gitlab-runner']['gitlab-runner']['repository'] = 'gitlab-runner'
default['cookbook-gitlab-runner']['gitlab-runner']['package'] = 'gitlab-runner'
default['cookbook-gitlab-runner']['gitlab-runner']['version'] = case node['platform_family']
                                                                when 'debian'
                                                                  '15.5.0'
                                                                when 'rhel'
                                                                  '15.5.0-1'
                                                                else
                                                                  nil
                                                                end

default['cookbook-gitlab-runner']['global_config'] = { "concurrent" => 1 }
default['cookbook-gitlab-runner']['runners'] = []
