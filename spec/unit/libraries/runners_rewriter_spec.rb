require 'spec_helper'

describe RunnersRewriter do
  let(:runners) do
    {
        'runner1' => {
            'executor' => 'docker',
            'machine' => {
                'MaxBuilds' => 1,
                'MachineOptions' => [
                    'option1=value1'
                ],
                'MachineOptionsMap' => {
                    'option2' => 'value2'
                }
            }
        }
    }
  end
  let(:expected_runner) do
    {
        'runner1' => {
            'executor' => 'docker',
            'machine' => {
                'MaxBuilds' => 1,
                'MachineOptions' => [
                    'option1=value1',
                    'option2=value2'
                ]
            }
        }
    }
  end

  subject { described_class.new(runners).rewrite }

  shared_examples 'runner rewriter' do
    it 'rewrites runner entries' do
      expect(subject).to eq(expected_runner)
    end
  end

  it_behaves_like 'runner rewriter'

  context 'when value contains =' do
    let(:runners) do
      {
          'runner1' => {
              'executor' => 'docker',
              'machine' => {
                  'MaxBuilds' => 1,
                  'MachineOptions' => [
                      'option1=value1=a'
                  ],
                  'MachineOptionsMap' => {
                      'option2' => 'value2=a'
                  }
              }
          }
      }
    end
    let(:expected_runner) do
      {
          'runner1' => {
              'executor' => 'docker',
              'machine' => {
                  'MaxBuilds' => 1,
                  'MachineOptions' => [
                      'option1=value1=a',
                      'option2=value2=a'
                  ]
              }
          }
      }
    end

    it_behaves_like 'runner rewriter'
  end

  context 'when map contains array as value' do
    let(:runners) do
      {
          'runner1' => {
              'executor' => 'docker',
              'machine' => {
                  'MaxBuilds' => 1,
                  'MachineOptionsMap' => {
                      'option1' => [
                          'value1',
                          'value2'
                      ]
                  }
              }
          }
      }
    end
    let(:expected_runner) do
      {
          'runner1' => {
              'executor' => 'docker',
              'machine' => {
                  'MaxBuilds' => 1,
                  'MachineOptions' => [
                      'option1=value1',
                      'option1=value2'
                  ]
              }
          }
      }
    end

    it_behaves_like 'runner rewriter'
  end

  context "when option doesn't contain value" do
    let(:runners) do
      {
          'runner1' => {
              'executor' => 'docker',
              'machine' => {
                  'MaxBuilds' => 1,
                  'MachineOptions' => [
                      'option1'
                  ],
                  'MachineOptionsMap' => {
                      'option2' => nil
                  }
              }
          }
      }
    end
    let(:expected_runner) do
      {
          'runner1' => {
              'executor' => 'docker',
              'machine' => {
                  'MaxBuilds' => 1,
                  'MachineOptions' => [
                      'option1',
                      'option2'
                  ]
              }
          }
      }
    end

    it_behaves_like 'runner rewriter'
  end
end
