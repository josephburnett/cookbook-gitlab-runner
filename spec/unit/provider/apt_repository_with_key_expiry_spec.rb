#
# Author:: Thom May (<thom@chef.io>)
# Copyright:: 2016-2018, Chef Software Inc.
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require "spec_helper"

# Now we are using the option --with-colons that works across old os versions
# as well as the latest (16.10). This for both `apt-key` and `gpg` commands
#
# Output of the command:
# => apt-key adv --list-public-keys --with-colons
APT_KEY_PUBLIC_KEYS = <<~EOF.freeze
  pub:e:4096:1:3F01618A51312F3F:1583162219:1646234219::-:::sc::::::23::0:
  uid:e::::1583162219::E590DAAB7BF1BD282E4C25DF888322A8135E8327::GitLab B.V. (package repository signing key) <packages@gitlab.com>::::::::::0:
  sub:e:4096:1:1193DC8C5FFF7061:1583162219:1646234219:::::e::::::23:
  pub:-:4096:1:3B4FE6ACC0B21F32:1336770936:::-:::scSC::::::23::0:
  uid:-::::1336770936::B7A02867A0C1D32B594B36C00E20C8C57E397748::Ubuntu Archive Automatic Signing Key (2012) <ftpmaster@ubuntu.com>::::::::::0:
  pub:-:4096:1:D94AA3F0EFE21092:1336774248:::-:::scSC::::::23::0:
  uid:-::::1336774248::77355A0B96082B2694009775B6490C605BD16B6F::Ubuntu CD Image Automatic Signing Key (2012) <cdimage@ubuntu.com>::::::::::0:
  pub:-:4096:1:871920D1991BC93C:1537196506:::-:::scSC::::::23::0:
  uid:-::::1537196506::BE438F08F546424C0EA810FD722053597EB5127B::Ubuntu Archive Automatic Signing Key (2018) <ftpmaster@ubuntu.com>::::::::::0:
EOF

# Output of the command:
# => gpg --with-colons [FILE]
GPG_PUBLIC_KEYS = <<~EOF.freeze
  pub:-:4096:1:3F01618A51312F3F:1583162219:1709313705::-:
  uid:::::::::GitLab B.V. (package repository signing key) <packages@gitlab.com>:
  sub:-:4096:1:1193DC8C5FFF7061:1583162219:1709313812:::
EOF

describe Chef::Provider::AptRepositoryWithKeyExpiry do
  let(:new_resource) { Chef::Resource::AptRepositoryWithKeyExpiry.new("multiverse") }

  let(:provider) do
    node = Chef::Node.new
    events = Chef::EventDispatch::Dispatcher.new
    run_context = Chef::RunContext.new(node, {}, events)
    Chef::Provider::AptRepositoryWithKeyExpiry.new(new_resource, run_context)
  end

  let(:apt_key_public_keys_cmd) do
    %w{apt-key adv --list-public-keys --with-colons}
  end

  let(:apt_key_public_keys) do
    double("shell_out", stdout: APT_KEY_PUBLIC_KEYS, exitstatus: 0, error?: false)
  end

  let(:apt_public_keys) do
    %w[pub:e:4096:1:3F01618A51312F3F:1583162219:1646234219
pub:-:4096:1:3B4FE6ACC0B21F32:1336770936:
pub:-:4096:1:D94AA3F0EFE21092:1336774248:
pub:-:4096:1:871920D1991BC93C:1537196506:]
  end

  it "responds to load_current_resource" do
    expect(provider).to respond_to(:load_current_resource)
  end

  describe "#is_key_id?" do
    it "detects a key" do
      expect(provider.is_key_id?("A4FF2279")).to be_truthy
    end
    it "detects a key with a hex signifier" do
      expect(provider.is_key_id?("0xA4FF2279")).to be_truthy
    end
    it "rejects a key with the wrong length" do
      expect(provider.is_key_id?("4FF2279")).to be_falsey
    end
    it "rejects a key with non-hex characters" do
      expect(provider.is_key_id?("A4KF2279")).to be_falsey
    end
  end

  describe "#extract_public_keys_from_cmd" do
    it "runs the desired command" do
      expect(provider).to receive(:shell_out).and_return(apt_key_public_keys)
      provider.extract_public_keys_from_cmd(*apt_key_public_keys_cmd)
    end

    it "returns a list of public keys" do
      expect(provider).to receive(:shell_out).and_return(apt_key_public_keys)
      expect(provider.extract_public_keys_from_cmd(*apt_key_public_keys_cmd)).to eql(apt_public_keys)
    end
  end

  describe "#cookbook_name" do
    it "returns 'test' when the cookbook property is set" do
      new_resource.cookbook("test")
      expect(provider.cookbook_name).to eq("test")
    end
  end

  describe "#no_new_keys?" do
    before do
      allow(provider).to receive(:extract_public_keys_from_cmd).with(*apt_key_public_keys_cmd).and_return(apt_public_keys)
    end

    let(:file) { "/tmp/remote-gpg-keyfile" }

    it "matches a set of keys" do
      allow(provider).to receive(:extract_public_keys_from_cmd)
        .with("gpg", "--with-colons", file)
        .and_return(Array(apt_public_keys.first))
      expect(provider.no_new_keys?(file)).to be_truthy
    end

    it "notices missing keys" do
      allow(provider).to receive(:extract_public_keys_from_cmd)
        .with("gpg", "--with-colons", file)
        .and_return(%w{ pub:-:4096:1:3F01618A51312F3F:1583162219:1709313705 })
      expect(provider.no_new_keys?(file)).to be_falsey
    end
  end

  describe "#key_type" do
    it "returns :remote_file with an http URL" do
      expect(provider.key_type("https://www.chef.io/key")).to eq(:remote_file)
    end

    it "returns :cookbook_file with a chef managed file" do
      expect(provider).to receive(:has_cookbook_file?).and_return(true)
      expect(provider.key_type("/foo/bar.key")).to eq(:cookbook_file)
    end

    it "throws exception if an unknown file specified" do
      expect(provider).to receive(:has_cookbook_file?).and_return(false)
      expect { provider.key_type("/foo/bar.key") }.to raise_error(Chef::Exceptions::FileNotFound)
    end
  end

  describe "#keyserver_install_cmd" do
    it "returns keyserver install command" do
      expect(provider.keyserver_install_cmd("ABC", "gpg.mit.edu")).to eq("apt-key adv --no-tty --recv --keyserver hkp://gpg.mit.edu:80 ABC")
    end

    it "uses proxy if key_proxy property is set" do
      new_resource.key_proxy("proxy.mycorp.dmz:3128")
      expect(provider.keyserver_install_cmd("ABC", "gpg.mit.edu")).to eq("apt-key adv --no-tty --recv --keyserver-options http-proxy=proxy.mycorp.dmz:3128 --keyserver hkp://gpg.mit.edu:80 ABC")
    end

    it "properly handles keyservers passed with hkp:// URIs" do
      expect(provider.keyserver_install_cmd("ABC", "hkp://gpg.mit.edu")).to eq("apt-key adv --no-tty --recv --keyserver hkp://gpg.mit.edu ABC")
    end
  end

  describe "#is_ppa_url" do
    it "returns true if the URL starts with ppa:" do
      expect(provider.is_ppa_url?("ppa://example.com")).to be_truthy
    end

    it "returns false if the URL does not start with ppa:" do
      expect(provider.is_ppa_url?("example.com")).to be_falsey
    end
  end

  describe "#repo_components" do
    it "returns 'main' if a PPA and components property not set" do
      expect(provider).to receive(:is_ppa_url?).and_return(true)
      expect(provider.repo_components).to eq("main")
    end

    it "returns components property if a PPA and components is set" do
      new_resource.components(["foo"])
      expect(provider).to receive(:is_ppa_url?).and_return(true)
      expect(provider.repo_components).to eq(["foo"])
    end

    it "returns components property if not a PPA" do
      new_resource.components(["foo"])
      expect(provider).to receive(:is_ppa_url?).and_return(false)
      expect(provider.repo_components).to eq(["foo"])
    end
  end

  describe "#install_ppa_key" do
    let(:url) { "https://launchpad.net/api/1.0/~chef/+archive/main" }
    let(:key) { "C5986B4F1257FFA86632CBA746181433FBB75451" }

    it "gets a key" do
      simples = double("HTTP")
      allow(simples).to receive(:get).and_return("\"#{key}\"")
      expect(Chef::HTTP::Simple).to receive(:new).with(url).and_return(simples)
      expect(provider).to receive(:install_key_from_keyserver).with(key, "keyserver.ubuntu.com")
      provider.install_ppa_key("chef", "main")
    end
  end

  describe "#make_ppa_url" do
    it "creates a URL" do
      expect(provider).to receive(:install_ppa_key).with("chef", "main").and_return(true)
      expect(provider.make_ppa_url("ppa:chef/main")).to eql("http://ppa.launchpad.net/chef/main/ubuntu")
    end
  end

  describe "#build_repo" do
    it "creates a repository string" do
      target = %Q{deb      "http://test/uri" unstable main\n}
      expect(provider.build_repo("http://test/uri", "unstable", "main", false, nil)).to eql(target)
    end

    it "creates a repository string with no distribution" do
      target = %Q{deb      "http://test/uri" main\n}
      expect(provider.build_repo("http://test/uri", nil, "main", false, nil)).to eql(target)
    end

    it "creates a repository string with source" do
      target = %Q{deb      "http://test/uri" unstable main\ndeb-src  "http://test/uri" unstable main\n}
      expect(provider.build_repo("http://test/uri", "unstable", "main", false, nil, true)).to eql(target)
    end

    it "creates a repository string with options" do
      target = %Q{deb      [trusted=yes] "http://test/uri" unstable main\n}
      expect(provider.build_repo("http://test/uri", "unstable", "main", true, nil)).to eql(target)
    end

    it "handles a ppa repo" do
      target = %Q{deb      "http://ppa.launchpad.net/chef/main/ubuntu" unstable main\n}
      expect(provider).to receive(:make_ppa_url).with("ppa:chef/main").and_return("http://ppa.launchpad.net/chef/main/ubuntu")
      expect(provider.build_repo("ppa:chef/main", "unstable", "main", false, nil)).to eql(target)
    end
  end
end
