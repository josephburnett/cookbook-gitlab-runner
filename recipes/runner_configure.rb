#
# Cookbook Name:: cookbook-gitlab-runner
# Recipe:: runner_configure
#
# Copyright 2016, GitLab Inc.
#

template 'config.toml' do
  path node['cookbook-gitlab-runner']['config_path']
  source 'config.toml.erb'
  owner 'root'
  group 'root'
  mode '0600'
  sensitive true
  helpers QuoteHelper
  variables(
    global: node['cookbook-gitlab-runner']['global_config'],
    runners: RunnersRewriter.new(node['cookbook-gitlab-runner']['runners']).rewrite
  )
end

cookbook_file 'cloudinit.sh' do
  path node['cookbook-gitlab-runner']['cloudinit_path']
  source 'cloudinit.sh'
  owner 'root'
  group 'root'
  mode '0700'
end

file node['cookbook-gitlab-runner']['gcp']['service_account_file'] do
  owner 'root'
  group 'root'
  mode '0600'
  content node['cookbook-gitlab-runner']['gcp']['service_account'].to_json
  not_if { node['cookbook-gitlab-runner']['gcp']['service_account'].empty? }
end

node['cookbook-gitlab-runner']['keys'].each do |key|
  next unless key['file']

  file key['file'] do
    owner 'root'
    group 'root'
    mode '0600'
    content key['content']
    not_if { key['content'].empty? }
  end
end
