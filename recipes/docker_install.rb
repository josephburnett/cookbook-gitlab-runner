#
# Cookbook Name:: cookbook-gitlab-runner
# Recipe:: docker install
#
# Copyright 2016, GitLab Inc.
#

if node['cookbook-gitlab-runner']['docker-ce']['install']
  case node['platform_family']
  when 'debian'
    apt_repository 'docker' do
      uri "https://download.docker.com/linux/#{node['platform']}"
      components ['stable']
      distribution node['lsb']['codename']
      key "https://download.docker.com/linux/#{node['platform']}/gpg"
    end
  when 'rhel'
    yum_repository 'docker' do
      description 'Docker yum repository'
      baseurl 'https://download.docker.com/linux/centos/docker-ce.repo'
      gpgkey 'https://download.docker.com/linux/centos/gpg'
    end
  end
end

# Remove obsolete Docker package.
package 'docker-engine' do
  action :remove
end

package 'docker-ce'

service 'docker' do
  supports [:restart]
end

directory '/etc/docker' do
  owner  'root'
  group  'root'
  mode   '0755'
end

template '/etc/docker/daemon.json' do
  owner  'root'
  group  'root'
  mode   '0755'
  notifies :restart, 'service[docker]', :delayed
end

remote_file '/usr/bin/docker-machine' do
  source node['cookbook-gitlab-runner']['docker-machine']['source']
  owner 'root'
  group 'root'
  mode '0755'
  checksum node['cookbook-gitlab-runner']['docker-machine']['checksum']
  only_if { node['cookbook-gitlab-runner']['docker-machine']['install'] }
  not_if "/usr/bin/docker-machine --version | grep #{node['cookbook-gitlab-runner']['docker-machine']['version']}"
end

# Restart Docker after iptables-persistent for nat rules
template '/etc/rc.local' do
  owner  'root'
  group  'root'
  mode   '0755'
end
