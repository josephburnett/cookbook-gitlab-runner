require 'serverspec'

# Required by serverspec
set :backend, :exec

describe "Git" do
  describe package('git') do
    it { should be_installed }
  end
end
