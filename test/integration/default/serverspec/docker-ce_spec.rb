require 'serverspec'

# Required by serverspec
set :backend, :exec

describe "Docker CE" do
  describe package('docker-ce') do
    it { should be_installed }
  end
end
