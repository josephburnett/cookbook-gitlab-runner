require 'serverspec'

# Required by serverspec
set :backend, :exec

describe "Docker machine" do
  describe command('docker-machine --version') do
    its(:stdout) { should match /docker-machine version 0.13.0/ }
  end
end
